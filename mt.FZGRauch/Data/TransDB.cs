﻿using System;
using System.Terminal;

namespace MT.FZGRauch.Data
{
    public static class TransDB
    {
        [Share("DD0501")]
        private static string IdTransData;
        [Share("DD0601")]
        private static string IdSupplier;
        [Share("DD0701")]
        private static string IdTransDataOut;
        
        public static void Init()
        {
            Database.Create();
            Database.CloseTables();
            Database.Open();
            ReadSupplier();
            Database.CloseTables();
        }

        public static bool Find(string WaagscheinNummer)
        {
            IdTransData = "";
            int r = TransData.SelectId(int.Parse(WaagscheinNummer).ToString());
            return (TransData.Current.Id != "");
        }

        public static bool FindOut(string WaagscheinNummer)
        {
            IdTransDataOut = "";
            int r = TransDataOut.SelectId(int.Parse(WaagscheinNummer).ToString());
            return (TransDataOut.Current.Id != "");
        }
        public static bool FindSupplier(string WCode)
        {
            IdSupplier = "";
            int r = Supplier.SelectId(int.Parse(WCode).ToString());
            return (Supplier.Current.Id != "");
        }

        public static void ReadSupplier()
        {
            string line;
            string mat;
            string name;
            string wcode;
            string licplate;


            File supplierFile = new File(5);
            supplierFile.Open("\\storage card\\TaskExpert\\Data\\Supplier.txt", FileAccess.Input);
            while (!supplierFile.AtEndOfStream)
            {
                supplierFile.ReadLine(out line);
                int pos = line.IndexOf(";");
                if (pos < 1)
                    return;
                wcode = line.Substring(1, pos - 1);
                line = line.Substring(pos + 1);
                pos = line.IndexOf(";");
                if (pos < 1)
                    return;
                mat = line.Substring(1, pos - 1);
                line = line.Substring(pos + 1);
                pos = line.IndexOf(";");
                if (pos < 1)
                    return;
                name = line.Substring(1, pos - 1);
                licplate = line.Substring(pos + 1);
                Supplier.Add(wcode, licplate, name, mat);
            }
            supplierFile.Close();
        }

    }
}

﻿using System;
using System.Diagnostics.Eventing;
using System.Reflection;
using System.Terminal;



namespace MT.FZGRauch.Logic
{
    public static class Printer
    {

        //        public static readonly SerialPort SerialClient = new SerialPort(SerialPortHandle, 1, AccessType.ReadWrite, 250, '\n', 1600, (SerialPortFlags)2);
        //public static readonly SerialPort SerialClient = new SerialPort(SerialPortHandle, 1, AccessType.ReadWrite, 250); 
        public static readonly SerialPort SerialClient = new SerialPort(1, 1, AccessType.Read, 80, '\n', 800, SerialPortFlags.AutoFlush);

        public static void WriteLine(string str)
        {
            SerialClient.Write(str);
        }

        public static PrinterStates CheckPrinter()
        {
            // return PrinterStates.Paper;
            string buf = "X";
            PrinterStates result = PrinterStates.NoPrinter;
            double t = DeviceTimer.GetTicks();
           
            SerialClient.ClearInputBuffer();
            SerialClient.Write("\x1b@");
            Task.Sleep(100);
            SerialClient.Write("\x10\x04\x05");
            Task.Sleep(100);
            SerialClient.Read(out buf);
            if (buf.Length > 0)
                result = (buf.Substring(1, 1) == String.Chr(0x12)) ? PrinterStates.Paper : PrinterStates.NoPaper;

            return (result);
        }

        public static PrinterStates FeedPaper()
        {
            if (CheckPrinter() == PrinterStates.Paper)
            {
                SerialClient.Write("\x0a");
                //Task.Sleep(100);
                return PrinterStates.FuncOk;
            }
            else
                return PrinterStates.FuncNotOk;
        }


        public static PrinterStates EjectPaper()
        {
            if (CheckPrinter() == PrinterStates.Paper)
            {
                Task.Sleep(100);
                SerialClient.Write("\x1b" + "FA");
                Task.Sleep(100);
                SerialClient.Write("\x0c");
                Task.Sleep(100);
                SerialClient.Write("\x1b" + "q");
                Task.Sleep(100);
                SerialClient.Write("\x1b" + "@");
                Task.Sleep(100);
                return PrinterStates.FuncOk;
            }
            else
                return PrinterStates.FuncNotOk;
        }
    }

}


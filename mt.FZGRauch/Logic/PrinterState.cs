﻿using System;
using System.Terminal;

namespace MT.FZGRauch.Logic
{
    public enum PrinterStates
    {
        NoPrinter = 1,
        NoPaper ,
        Paper,
        FuncOk  ,
        FuncNotOk
    }
}

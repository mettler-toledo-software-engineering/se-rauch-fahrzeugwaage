﻿using System;
using System.Terminal;

namespace MT.FZGRauch.Logic
{
    public static class Scale
    {
        // Select Scale 1 
        [Share("qc0131")]
        private static bool SelScale1;

        // AlibiNr
        [Share("xp0101")]
        private static int AlibiNrSD;

        //Scale 1
        [Share(SD.Scale.Status, 1, 31)]
        private static bool inMotion_1;
        [Share(SD.Scale.DynamicWeight, 1, 1)]
        private static string grossStr_1;
        [Share(SD.Scale.DynamicWeight, 1, 3)]
        public static string Unit_1;
        [Share(SD.Scale.DynamicWeight, 1, 10)]
        private static double gross_1;
        [Share(SD.Scale.Setup, 1, 3)]
        private static string Name_1;
        [Share(SD.Scale.Commands, 1, 03)] // print command
        private static int alibiprint_1;
        [Share(SD.Scale.Status, 1, 3)] // print command
        private static int pStatus_1;
        [Share(SD.Scale.Status, 1, 33)] // overload
        private static int Overload_1;

        [Share("ce0105")] // Auflöung Waage 1 (Achse)
        public static int Resolution_1;

        [Share("ce0110")] // Max Waage 1 (Achse)
        public static int MaxLoad_1;

        [Share("ce0505")] // Auflöung Waage 1 (lang)
        public static int Resolution_5;

        [Share("ce0510")] // Max Waage 1+2 Sum (lang)
        public static int MaxLoad_5;
        // Letztes Eichdatum
        [Share(SD.Application.SetupString, 10)] // print command
        public static string LastApproval;


        [Share("xt0101")] // current scale
        private static int CurrentScale;

        [Share("xs0113")]
        public static string TimeSeparator;
        [Share("xd0104")]
        public static string TimeOfDay;

        public static string GetDateTime()
        {
            string timeStamp;
            string d, year, month, day;
            TimeSeparator = ":";
            d = Environment.Date;
            year = d.Left(4);
            month = d.Substring(6, 2);
            day = d.Substring(9, 2);
            timeStamp = day + "." + month;
            timeStamp += "." + year + " ";
            timeStamp += TimeOfDay;
            return timeStamp;
        }
        public static string GetDate()
        {
            string timeStamp;
            string d, year, month, day;
            TimeSeparator = ":";
            d = Environment.Date;
            year = d.Left(4);
            month = d.Substring(6, 2);
            day = d.Substring(9, 2);
            timeStamp = day + "." + month;
            timeStamp += "." + year;
            return timeStamp;
        }
        public static string GetTime()
        {
            TimeSeparator = ":";
            return TimeOfDay;
        }

        public static void SelectScale(ScaleNo type)
        {
            switch (type)
            {
                case ScaleNo.Achsen:
                    SelScale1 = true;
                    break;
                default:
                    break;
            }
        }

        public static void AlibiPrint()
        {
            switch (GetCurrentScale())
            {
                case ScaleNo.Achsen:
                    while (inMotion_1)
                        Task.Sleep(100);
                    grossStrFreeze = grossStr_1;
                    grossFreeze = gross_1;
                    overload = Overload_1;
                    alibiprint_1 = 1;
                    timeStampDate = GetDate();
                    timeStampTime = GetTime();
                    alibiNr = AlibiNrSD;
                    while (alibiprint_1 == 1) Task.Sleep(100);
                    while (pStatus_1 == 1)
                        Task.Sleep(100);

                    unit = Unit_1;
                    scaleName = Name_1;
                    break;
                default:
                    break;
            }
        }

        public static int SelectedScale
        {
            get
            {
                return (int)GetCurrentScale();
            }
        }

        public static string GetMinWeight
        {
            get
            {
                string s;
                switch (GetCurrentScale())
                {
                    case ScaleNo.Achsen:
                        s = (Resolution_1 * 20).ToString();
                        s += Unit_1;
                        return s.TrimStart();
                    default:
                        return "";
                }
            }

        }

        public static string GetResolution
        {
            get
            {
                string s;
                switch (GetCurrentScale())
                {
                    case ScaleNo.Achsen:
                        s = Resolution_1.ToString();
                        s += " " + Unit_1;
                        return s.TrimStart();
                    default:
                        return "";
                }
            }

        }

        public static string GetMaxLoad
        {
            get
            {
                string s;
                switch (GetCurrentScale())
                {
                    case ScaleNo.Achsen:
                        s = MaxLoad_1.ToString();
                        s += Unit_1; ;
                        return s.TrimStart();
                   default:
                        return "";
                }
            }

        }
        private static int alibiNr;
        public static int AlibiNr
        {
            [Inline]
            get
            {
                return alibiNr;
            }
        }
        private static string unit;
        public static string Unit
        {
            [Inline]
            get
            {
                return unit.TrimStart();
            }
        }
        private static string grossStrFreeze;
        public static string GrossStrFreeze
        {
            [Inline]
            get
            {
                return grossStrFreeze.TrimStart();
            }
        }
        private static double grossFreeze;
        public static double GrossFreeze
        {
            [Inline]
            get
            {
                return grossFreeze;
            }
        }

        private static string timeStampDate;
        public static string TimeStampDate
        {
            [Inline]
            get
            {
                return timeStampDate;
            }
        }

        private static string timeStampTime;
        public static string TimeStampTime
        {
            [Inline]
            get
            {
                return timeStampTime;
            }
        }
        private static string scaleName;
        public static string ScaleName
        {
            [Inline]
            get
            {
                return scaleName;
            }
        }
        private static int overload;
        public static int Overload
        {
            [Inline]
            get
            {
                return overload;
            }
        }

        public static double Gross
        {
            [Inline]
            get
            {
                switch (CurrentScale)
                {
                    case 1:
                        return gross_1;
                   default:
                        break;
                }
                return 0;
            }
        }

        public static ScaleNo GetCurrentScale()
        {
            switch (CurrentScale)
            {
                case 1:
                    return ScaleNo.Achsen;
                default:
                    return ScaleNo.Achsen;
            }
        }

        public enum ScaleNo
        {
            /// <summary>
            /// Search after the column name
            /// </summary>			
            Achsen = 1
        }
    }
}

﻿
using System;
using System.Terminal;
using MT.FZGRauch.Data;

namespace MT.FZGRauch.Logic
{
    public static class TcpClient
    {
        [Share(SD.Application.DynamicString, 2)]
        public static string ActionState;

        [Share(SD.Scale.Setup, 1, 3)]
        public static string ScaleId;
        public static Socket client;

        private static bool Connect()
        {
            if (client.GetStatus == SocketStatus.Success)
                return true;
            //  LogFile.WriteSocketStatus("before connect");
            client = Socket.Connect(Program.ServerIP, Program.ServerPort);
            if (!client.IsValid)
            {
                Display.Popup("TCPClient", "Keine Verbindung", "zum Server!");
                return false;
            }
            return true;
        }

        public static void Close()
        {
            client.Close();
            Task.Sleep(200);
        }


        // LeaveOrder
        public static string SendWeigh()
        {
            bool quit = false;
            string res = "";
            string string1;
            string string2;
            string string3;

            Database.Open();
            TransDB.FindOut(int.Parse(Program.WaagScheinNrDisp).ToString());
            do
            {   // if you change here the code, you NEED also change the receiver code....
                if (Connect())
                {
                    string1 = Scale.GetMaxLoad + ";" + Scale.GetResolution + ";";
                    string1 += Scale.GetMinWeight + ";" + Scale.LastApproval;

                    string2 = TransDataOut.Current.WaagScheinNummer + ";";
                    string2 += TransDataOut.Current.WCode + ";";
                    string2 += TransDataOut.Current.LicPlate.Left(15) + ";";
                    string2 += TransDataOut.Current.Name.Left(25) + ";";
                    string2 += TransDataOut.Current.Material.Left(20) + ";";
                    string2 += TransDataOut.Current.FirstDate + ";";
                    string2 += TransDataOut.Current.FirstTime;

                    string3 = TransDataOut.Current.FirstAlibiNr + ";";
                    string3 += TransDataOut.Current.FirstBruttoStr + ";";
                    string3 += TransDataOut.Current.SecondDate + ";";
                    string3 += TransDataOut.Current.SecondTime.Left(15) + ";";
                    string3 += TransDataOut.Current.SecondAlibiNr + ";";
                    string3 += TransDataOut.Current.SecondBruttoStr;


                    client.Write("#XRauchPrinter.ReceiveData???");
                    client.Write(string.Chr(8));
                    client.Write(string1);
                    client.Write(string.Chr(8));
                    client.Write(string2);
                    client.Write(string.Chr(8));
                    client.Write(string3);
                    client.Write("\f");

                    double tEnd = DeviceTimer.GetTicks() + 2 /* sec */;

                    string result = "";
                    do
                    {
                        result += client.Read(50, 50);
                        if (result.IndexOf("\f") > 1)
                            quit = true;
                    }
                    while (!quit && tEnd > DeviceTimer.GetTicks());
                    if (quit)
                    {
                        // receive ok
                        TcpClient.Close();
                        //    LogFile.WriteLog(2, "Connection closed", "");
                        int pos = result.IndexOf("#S");
                        if (pos > 0 && result.Length > 2)
                        {
                            int FormFeed = result.IndexOf("\f");
                            //string x = FormFeed.ToString() + "/" + result.Length.ToString();
                            if (result != "" && FormFeed == result.Length)
                            {
                                res = result.Substring(pos + 2, FormFeed - pos - 2);
                            }

                        }
                    }
                    else
                    {
                        client.Close();
                    }
                }
                else
                {
                    quit = true;
                    client.Close();
                }
            }
            while (!quit);
            Database.CloseTables();
            return (res);
        }

    }
}

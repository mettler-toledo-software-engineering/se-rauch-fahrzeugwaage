﻿using System;
using System.Security.Cryptography;
using System.Terminal;
using MT.FZGRauch.Data;
using MT.FZGRauch.Pages;


namespace MT.FZGRauch.Logic
{
    public static class Templates
    {

        const string CopyRight = "(c) 2022 Mettler-Toledo (Schweiz) GmbH";

        public static void PrintPaper()
        {
            string ue = string.Chr(252);
            int i;
            Database.Open();
            TransDB.FindOut(int.Parse(Program.WaagScheinNrDisp).ToString());

            double brutto = double.Parse(TransDataOut.Current.FirstBruttoStr);
            double tara = double.Parse(TransDataOut.Current.SecondBruttoStr);
            int netto = Math.Int(brutto - tara);
       
            File file = new File(3);
            file.Open("\\storage card\\TaskExpert\\Data\\templ.txt", FileAccess.Output);
            //file.Open("templ.txt", FileAccess.Output);
           
            TransDB.FindOut(Program.WaagScheinNrDisp);
            file.WriteLine("Transaktion" + string.Space(19) + TransDataOut.Current.WaagScheinNummer.TrimStart().TrimEnd().PadLeft(5, "0"));
            file.WriteLine("Warencode" + string.Space(24) + TransDataOut.Current.WCode.TrimStart().TrimEnd().PadLeft(2, "0"));
            i = TransData.Current.Name.TrimStart().TrimEnd().Length;
            file.WriteLine(string.Space(35-i) + TransDataOut.Current.Name.TrimStart().TrimEnd());
            i = TransData.Current.LicPlate.TrimStart().TrimEnd().Length;
            file.WriteLine("KFZ-Kennzeichen" + string.Space(20 - i) + TransData.Current.LicPlate.TrimStart().TrimEnd());
            file.WriteLine();
            file.WriteLine("1.Wiegung");
            i = TransData.Current.FirstDate.Length + TransDataOut.Current.FirstTime.Length;
            file.WriteLine(TransDataOut.Current.FirstDate + string.Space(35 - i) + TransDataOut.Current.FirstTime);
            file.WriteLine("Alibi-Nr." + string.Space(20) + TransDataOut.Current.FirstAlibiNr.TrimStart().TrimEnd().PadLeft(6, "0"));
            file.WriteLine("\x1b\x21\x10");  // Double Heigth Mode on
            file.WriteLine("1.Gewicht" + string.Space(18) + TransDataOut.Current.FirstBruttoStr.PadLeft(5, " ") + " kg");
            file.WriteLine("\x1b\x21\x40");  // Double Heigth Mode off

            file.WriteLine();
            file.WriteLine("2.Wiegung");
            i = TransDataOut.Current.SecondDate.Length + TransDataOut.Current.SecondTime.Length;
            file.WriteLine(TransDataOut.Current.SecondDate + string.Space(35 - i) + TransDataOut.Current.SecondTime);
            file.WriteLine("Alibi-Nr." + string.Space(20) + TransDataOut.Current.SecondAlibiNr.TrimStart().TrimEnd().PadLeft(6, "0"));
            file.WriteLine("\x1b\x21\x10");  // Double Heigth Mode on
            file.WriteLine("2.Gewicht" + string.Space(18) + TransDataOut.Current.SecondBruttoStr.PadLeft(5, " ") + " kg");
            file.WriteLine("-".Repeat(35));
            file.WriteLine("Netto" + string.Space(22) + netto.ToString().TrimStart().PadLeft(5," ") + " kg"); 
            file.WriteLine("\x1b\x21\x40");  // Double Heigth Mode off

            Database.CloseTables();
            file.Close();
        }

        public static void PrintEinfachwaegung(int WNr)
        {
            int i;
           
            File file = new File(3);
            file.Open("\\storage card\\TaskExpert\\Data\\templ.txt", FileAccess.Output);
            //file.Open("templ.txt", FileAccess.Output);
            file.WriteLine("\x1b\x21\x10");  // Double Heigth Mode on
            file.WriteLine("      **** EINFACHWAEGUNG ****");
            file.WriteLine("\x1b\x21\x40");  // Double Heigth Mode off
            file.WriteLine("\n\n");
            
            file.WriteLine("Transaktion" + string.Space(19) + WNr.ToString().TrimStart().TrimEnd().PadLeft(5, "0"));
            i = Program.Identification.Length;
            file.WriteLine("KFZ-Kennzeichen" + string.Space(20 - i) + Program.Identification.TrimStart().TrimEnd());
            file.WriteLine();
            i = Scale.TimeStampTime.Length + Scale.TimeStampDate.Length;
            file.WriteLine(Scale.TimeStampDate + string.Space(35 - i) + Scale.TimeStampTime);
            file.WriteLine("Alibi-Nr." + string.Space(20) + Scale.AlibiNr.ToString().TrimStart().TrimEnd().PadLeft(6, "0"));
            file.WriteLine("\x1b\x21\x10");  // Double Heigth Mode on
            file.WriteLine("Gewicht" + string.Space(20) + Scale.GrossStrFreeze.PadLeft(5, " ") + " kg");
            file.WriteLine("-".Repeat(35));
            file.WriteLine("\x1b\x21\x40");  // Double Heigth Mode off

            file.Close();
        }

        public static void PrintClients()
        {
            string temp;
            int i;
            Database.Open();
            Supplier.SelectAllRows();
            File file = new File(3);
            file.Open("\\storage card\\TaskExpert\\Data\\templ.txt", FileAccess.Output);
            file.WriteLine("\x1b\x21\x10");  // Double Heigth Mode on
            //file.WriteLine("      **** EINFACHWAEGUNG ****");
            file.WriteLine("      ****     ARTIKEL    ****");
            file.WriteLine("\x1b\x21\x40");  // Double Heigth Mode off
            file.WriteLine("\n");
            do  //35
            {
                temp = Supplier.Current.WCode.TrimStart().TrimEnd().PadLeft(2, "0");
                //i = Supplier.Current.Material.Length;
                //file.WriteLine(string.Space(26 - i) +  Supplier.Current.Material.TrimStart().TrimEnd());
                //file.Write(Supplier.Current.Name.TrimStart().TrimEnd());
                //i = Supplier.Current.Name.Length + Supplier.Current.LicPlate.Length;
                //file.WriteLine(string.Space(35 - i) + Supplier.Current.LicPlate.TrimStart().TrimEnd());
                temp += " / " + Supplier.Current.Material.TrimStart().TrimEnd();
                temp += " / " + Supplier.Current.Name.TrimStart().TrimEnd(); 
                temp += " / " + Supplier.Current.LicPlate.TrimStart().TrimEnd();
                i = (temp.Length > 35) ? 35 : temp.Length;
                file.WriteLine(temp.Substring(1,i));
            } while (Supplier.NextRow() != 0);
            Database.CloseTables();
            file.Close();
        }

    }

}

﻿using System;
using System.Linq.Expressions;
using System.Terminal;
using MT.FZGRauch.Data;
using MT.FZGRauch.Logic;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for Artikel
    /// </summary>
    public static partial class Artikel
    {
        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        static partial void Initialize()
        {
            Database.Open();
            Supplier.SelectAllRows();
            Program.Title = "Artikelverwaltung";
            Program.FirstIdentification = Supplier.Current.LicPlate;
            Program.Warencode = Supplier.Current.WCode;
            Program.Hinweis = Supplier.Current.Name;
            Program.FirstTimeStamp = Supplier.Current.Material;
        }

        static partial void cmdExit_Triggered()
        {
            ReturnToMainScreen();
        }

        private static void ReturnToMainScreen()
        {
            Database.CloseTables();
            Program.ActionPointer = new Program.Action(MainScreen.Show);
            Close();
        }

        static partial void cmdNext_Triggered()
        {
            if (Supplier.NextRow() == 0)
                Supplier.SelectAllRows();
            Program.FirstIdentification = Supplier.Current.LicPlate;
            Program.Warencode = Supplier.Current.WCode;
            Program.Hinweis = Supplier.Current.Name;
            Program.FirstTimeStamp = Supplier.Current.Material;
            _redraw = true;

        }

        static partial void cmdOk_Triggered()
        {
            bool exist = false;
            if (Program.Warencode != "")
            {
                exist = TransDB.FindSupplier(Program.Warencode);
                if (exist)
                {
                    Supplier.Current.LicPlate = txtKFZ.Text;
                    Supplier.Current.Name = txtName2.Text;
                    Supplier.Current.Material = txtMaterial.Text;
                    Program.ActionPointer = new Program.Action(MainScreen.Show);
                    Close();
                }
                Database.CloseTables();
            }
        }

        static partial void cmdPrint_Triggered()
        {
            Program.Title = "Bitte warten...";
            Database.CloseTables();
            Task.Sleep(200);

            Templates.PrintClients();
            Program.WaagScheinNrDisp = "";
            Program.ActionPointer = new Program.Action(Printing.Show);
            Close();
        }

        //static partial void cmdTransClr_Triggered()
        //{
        //    Database.CloseTables();
        //    Program.Title = "Artikel l" + string.Chr(246) + "schen ?";

        //    Program.ActionPointer = new Program.Action(YesNo.Show);
        //    Close();
        //}
    }
}

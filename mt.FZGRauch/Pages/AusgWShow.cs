﻿using System;
using System.Management.Instrumentation;
using System.Terminal;
using MT.FZGRauch.Data;
using MT.FZGRauch.Logic;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for AusgWShow
    /// </summary>
    public static partial class AusgWShow
    {

        private static bool first;
        public static bool Show()
        {
            ShowDialog();
            return false;
        }
        static partial void Initialize()
        {
            Program.Title = "Werte aus Eingangsw" + string.Chr(228) + "gung";
            first = true;
            Program.Hinweis = "";
        }

        static partial void cmdExit_Triggered()
        {
            Program.ActionPointer = new Program.Action(MainScreen.Show);
            Close();
        }

        static partial void cmdScale_Triggered()
        {
           string ae = string.Chr(228);
            if (first)
            {
                first = false;
                Scale.AlibiPrint();
                if (Scale.Overload == 1)
                    Display.Popup("W" + ae + "gen", "Waage nicht bereit", "Keine Erfassung");
                else
                {
                    Database.Open();
                    TransDB.Find(Program.WaagScheinNrDisp);
                    TransDataOut.Add(int.Parse(Program.FirstWaagscheinNummer).ToString(), Program.Warencode, Program.FirstIdentification,
                        TransData.Current.Name, TransData.Current.Material, TransData.Current.FirstBruttoStr,
                        TransData.Current.FirstDate,
                        TransData.Current.FirstTime, TransData.Current.FirstAlibiNr, Scale.GrossStrFreeze,
                        Scale.TimeStampDate,
                        Scale.TimeStampTime, Scale.AlibiNr.ToString());
                    TransData.DeleteId(int.Parse(Program.FirstWaagscheinNummer).ToString());
                    Database.CloseTables();
                    Task.Sleep(200);

                    Templates.PrintPaper();

                    if ((Program.Laser == 0) && (Program.Local == 0))
                    {
                        Program.Title = "Kein Druck verlangt! L" + string.Chr(246) + "schen ?";
                        Program.ActionPointer = new Program.Action(YesNo.Show);
                        Close();
                    }
                    else
                    {
                        Program.ActionPointer = new Program.Action(Printing.Show);
                        Close();
                    }
                }
            }
        }
    }
}

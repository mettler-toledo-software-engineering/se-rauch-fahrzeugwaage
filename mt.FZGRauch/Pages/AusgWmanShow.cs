﻿using System;
using System.Terminal;
using MT.FZGRauch.Logic;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for AusgWmanShow
    /// </summary>
    public static partial class AusgWmanShow
    {
        private static bool first;

        static partial void Initialize()
        {
            first = true;
            Program.Hinweis = "";
            Program.Title = "Manuelle Eingaben";

        }
        public static bool Show()
        {
            ShowDialog();
            return false;
        }
        static partial void cmdReport_Triggered()
        {
            Program.Hinweis = "Waagschein wird nachgedruckt...";
            Printing.RePrint(true);
            Program.Hinweis = "Waagschein gedruckt";
        }


        static partial void cmdWaegen_Triggered()
        {
            string ae = string.Chr(228);
            if (first)
            {
                first = false;
                Scale.AlibiPrint();
                if(Scale.Overload == 1)
                    Display.Popup("W" + ae + "gen", "Waage nicht bereit", "Keine Erfassung");
                else
                {
                    Program.Hinweis = "Waagschein wird gedruckt...";
                  //  Templates.PrintAusgangsswaegung();
                    SoftKeys.Clear();
                    SoftKeys.Define(Keys.SoftKey1, 100, "\\exit.bmp");
                    SoftKeys.Define(Keys.SoftKey3, 102, "\\report.bmp");
                    //              SoftKeys.Define(Keys.SoftKey5, 104, "\\waegen.bmp");
                    SoftKeys.Replace();
                    Program.Hinweis = "Waagschein gedruckt";
                }
            }

        }

        static partial void cmdExit_Triggered()
        {
            Program.ActionPointer = new Program.Action(MainScreen.Show);
            Close();
        }
    }
}

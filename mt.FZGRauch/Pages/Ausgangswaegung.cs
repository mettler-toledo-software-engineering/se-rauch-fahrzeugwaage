﻿using System;
using System.Linq.Expressions;
using System.Terminal;
using MT.FZGRauch.Data;
using MT.FZGRauch.Logic;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for Ausgangswaegung
    /// </summary>
    public static partial class Ausgangswaegung
    {

        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        static partial void Initialize()
        {
            Program.Title = "Ausgangsw" + string.Chr(228) + "gung";
            Program.FirstIdentification = TransData.Current.LicPlate;
            Program.WaagScheinNrDisp = TransData.Current.WaagScheinNummer;
        }

        static partial void cmdExit_Triggered()
        {
            ReturnToMainScreen();
        }

        private static void ReturnToMainScreen()
        {
            Database.CloseTables();
            Program.ActionPointer = new Program.Action(MainScreen.Show);
            Close();
        }

        static partial void cmdNext_Triggered()
        {
            if (TransData.NextRow() == 0)
                TransData.SelectAllRows();
            Program.FirstIdentification = TransData.Current.LicPlate;
            Program.WaagScheinNrDisp = TransData.Current.WaagScheinNummer;
            _redraw = true;

        }

        static partial void cmdOk_Triggered()
        {
            bool exist = false;
            Database.CloseTables();
            if (Program.WaagScheinNrDisp != "")
            {
                Database.Open();
                exist = TransDB.Find(Program.WaagScheinNrDisp);
                if (!exist)
                {
                    Program.FirstWeighingExist = false;
                    Program.ActionPointer = new Program.Action(ManualInput.Show);
                    Close();
                }
                else
                {
                    Program.FirstWeighingExist = true;
                    Program.FirstWaagscheinNummer = TransData.Current.WaagScheinNummer.ToString().TrimStart().PadLeft(4, "0");
                    Program.FirstTimeStamp = TransData.Current.FirstDate + " " + TransData.Current.FirstTime;
                    Program.FirstWeight = TransData.Current.FirstBruttoStr.PadLeft(5, "0") + " kg";
                    Program.FirstIdentification = TransData.Current.LicPlate;
                    Program.Warencode = TransData.Current.WCode;
                    Program.ActionPointer = new Program.Action(AusgWShow.Show);
                    Close();
                }
                Database.CloseTables();
            }
        }

        static partial void cmdTransClr_Triggered()
        {
            Database.CloseTables();
            Program.Title = "Transaktion l" + string.Chr(246) +"schen ?";
            Program.ActionPointer = new Program.Action(YesNo.Show);
            Close();
        }
    }
}

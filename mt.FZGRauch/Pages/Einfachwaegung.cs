﻿using System;
using System.Terminal;
using MT.FZGRauch.Data;
using MT.FZGRauch.Logic;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for Einfachwaegung
    /// </summary>
    public static partial class Einfachwaegung
    {
        public static bool Show()
        {
            ShowDialog();
            return false;
        }
        static partial void Initialize()
        {
            Program.Title = "Einfachw" + string.Chr(228) + "gung";
            Program.Hinweis = "";
        }

        static partial void cmdExit_Triggered()
        {
            Program.ActionPointer = new Program.Action(MainScreen.Show);
            Close();
        }

        static partial void cmdWaegen_Triggered()
        {
            string ae = string.Chr(228);

            // hier wägen
            Scale.AlibiPrint();
            if (Scale.Overload == 1)
                Display.Popup("W" + ae + "gen", "Waage nicht bereit", "Keine Erfassung");
            else
            {
                Database.Open();
                Program.Identification = txtIdentification.Text;
                                TransDataOut.Add(Program.WaagscheinNummer.ToString(), "", Program.Identification,
                        "NoName", "NoMaterial", Scale.GrossStrFreeze, Scale.TimeStampDate,
                        Scale.TimeStampTime, Scale.AlibiNr.ToString(), Scale.GrossStrFreeze,
                        Scale.TimeStampDate, Scale.TimeStampTime, Scale.AlibiNr.ToString());
                Templates.PrintEinfachwaegung(Program.WaagscheinNummer);
                Program.WaagScheinNrDisp = Program.WaagscheinNummer.ToString();
                Program.WaagscheinNummer++;
                Database.CloseTables();
                              
                if ((Program.Laser == 0) && (Program.Local == 0))
                {
                    Program.Title = "Kein Druck verlangt! L" + string.Chr(246) + "schen ?";
                    Program.ActionPointer = new Program.Action(YesNo.Show);
                    Close();
                }
                else
                {
                    Program.ActionPointer = new Program.Action(Printing.Show);
                    Close();
                }

            }
        }
    }
}

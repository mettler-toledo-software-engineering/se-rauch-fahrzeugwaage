﻿using System;
using System.Terminal;
using MT.FZGRauch.Data;
using MT.FZGRauch.Logic;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for Eingangswaegung
    /// </summary>
    public static partial class Eingangswaegung
    {
        private static bool done;
        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        static partial void Initialize()
        {
            Program.Title = "Eingangsw" + string.Chr(228) + "gung";
            done = false;
            Program.dummy = true;  ////NICHTS ändern, Compiler dreht durch   txtWarencode muss INDEX 5 haben da sonst der EVENT nicht kommt
            Program.Hinweis = "";
        }

        static partial void cmdExit_Triggered()
        {
            txtWarencode.TextChanged += None;
            Program.ActionPointer = new Program.Action(MainScreen.Show);
            Close();
        }

        static partial void BeforePaint()
        {
            txtWarencode.TextChanged += None;
        }
        static partial void Closed()
        {
            txtWarencode.TextChanged += None;
        }

        static void None()
        {
        }

        static partial void AfterPaint()
        {
            txtWarencode.TextChanged += txtNrOperation_TextChanged;
        }

        static void txtNrOperation_TextChanged()
        {
            bool exist = false;
            if (txtWarencode.Text != "")
            {
                Database.Open();
                exist = TransDB.FindSupplier(txtWarencode.Text);

                if (!exist)
                {
                    Program.ActionPointer = new Program.Action(MainScreen.Show);
                    Close();
                }
                else
                {
                    txtIdentification.Draw(Supplier.Current.LicPlate);
                    Program.Hinweis = Supplier.Current.Material + " / " + Supplier.Current.Name;
                }
                Database.CloseTables();
            }
        }

        static partial void cmdWaegen_Triggered()
        {
            string ae = string.Chr(228);
            if (done == false)
            {
                done = true;
                Scale.AlibiPrint();
                if (Scale.Overload == 1)
                    Display.Popup("W" + ae + "gen", "Waage nicht bereit", "Keine Erfassung");
                else
                {
                    Program.Identification = txtIdentification.Text;
                    Database.Open();
                    TransData.Add(Program.WaagscheinNummer, Supplier.Current.WCode, Program.Identification,
                        Supplier.Current.Name, Supplier.Current.Material, Scale.GrossStrFreeze,
                        Scale.TimeStampDate, Scale.TimeStampTime, Scale.AlibiNr.ToString());
                    Database.CloseTables();
                    Program.WaagscheinNummer++;
                    Program.Hinweis = "Gewicht erfasst!";
                    Task.Sleep(1000);
                    txtWarencode.TextChanged += None;
                    Program.ActionPointer = new Program.Action(MainScreen.Show);
                    Close();

                }
            }
        }



    }
}

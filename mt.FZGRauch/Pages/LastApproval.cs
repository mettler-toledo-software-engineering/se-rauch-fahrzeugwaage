﻿using System;
using MT.FZGRauch.Logic;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for LastApproval
    /// </summary>
    public static partial class LastApproval
    {
        public static bool Show()
        {
            ShowDialog();
            return false;
        }
        static partial void Initialize()
        {
            txtDate.Text = Scale.LastApproval;
        }
        static partial void cmdExit_Triggered()
        {
            Program.ActionPointer = new Program.Action(Settings.Show);
            Close();
        }

        static partial void cmdOk_Triggered()
        {
            Scale.LastApproval = txtDate.Text;
            Program.ActionPointer = new Program.Action(Settings.Show);
            Close();
        }

       

    }
}

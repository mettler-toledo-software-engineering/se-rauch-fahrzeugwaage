// This code is autogenerated by the Runtime Screen Designer
// Do not modify this code, your changes will be lost when using the designer view

using System;
using System.Terminal;

namespace MT.FZGRauch.Pages
{
	public static partial class MainScreen
	{
		/// <summary>
		/// Internal. Stores if the dialog should be closed after the next event
		/// </summary>	
		private static bool _closing = false;
	
		/// <summary>
		/// Internal. Stores if the dialog should be repainted after the next event
		/// </summary>			
		private static bool _redraw = false;
		/// <summary>
		/// Stores the label1 control
		/// </summary>					
		private static readonly Label label1 = new Label(1, 60, 14, "Fahrzeugwaagen-Terminal", Fonts.Normal16, Colors.Black);
		/// <summary>
		/// Stores the label3 control
		/// </summary>					
		private static readonly Label label3 = new Label(2, 113, 59, "P21092201", Fonts.Normal10, Colors.Black);
		/// <summary>
		/// Stores the sharedDataLabel1 control
		/// </summary>					
		public static readonly SharedDataLabel<string> sharedDataLabel1 = new SharedDataLabel<string>(3, 174, 59, "AK0101", "!20", Fonts.Normal10, Colors.Black);
		/// <summary>
		/// Stores the sharedDataLabel2 control
		/// </summary>					
		private static readonly SharedDataLabel<string> sharedDataLabel2 = new SharedDataLabel<string>(4, 92, 36, "AK0102", "!29", Fonts.Normal16, Colors.Black);
							
		/// <summary>
		/// Displays the dialog
		/// </summary>
		public static void ShowDialog()
		{
			_closing = false;
			_redraw = true;
			Initialize();
			do
			{			
				if (_redraw)
				{
					BeforePaint();
					InitializeComponents();
					AfterPaint();
					_redraw = false;
				}			
				
				Input.DoEvents();
				EventOccured();
				
				switch(RuntimeScreen.ReadKey())
				{
					case 100:
						cmdEingang_Triggered();
						break;
					case 102:
						cmdEinfach_Triggered();
						break;
					case 104:
						cmdAusgang_Triggered();
						break;
					case 105:
						cmdSettings_Triggered();
						break;
				
				}
			} while (!_closing);
			Closed();
		}
		
		private static void InitializeComponents()
		{
			Display.Clear();
			Display.SetupWeightDisplay(WeightDisplayVisibility.On, WeightDisplayScale.Active, WeightDisplayTare.Tare, WeightDisplayCompress.Uncompress, WeightDisplayScaleSize.Medium, WeightDisplayScaleSize.Medium);

		    label1.Draw();
		    label3.Draw();
		    sharedDataLabel1.Draw();
		    sharedDataLabel2.Draw();
			
		
			// Update softkeys
			SoftKeys.Clear();
			SoftKeys.Define(Keys.SoftKey1, 100, "\\eingw.bmp");
			SoftKeys.Define(Keys.SoftKey3, 102, "\\einfw.bmp");
			SoftKeys.Define(Keys.SoftKey5, 104, "\\ausgw.bmp");
			SoftKeys.Define(Keys.SoftKey6, 105, "\\Setparam.bmp");
							
			DefineSoftkeys();
			SoftKeys.Replace();
		}
		
		/// <summary>
		/// Closes the dialog
		/// </summary>
		[Inline]
		public static void Close()
		{
			_closing = true;
		}
		
		/// <summary>
		/// Forces a redraw of the runtime screen
		/// </summary>
		[Inline]
		private static void Redraw()
		{
			_redraw = true;
		}
		
		/// <summary>
		/// Occures when the dialog is initialized
		/// </summary>
		static partial void Initialize();		

		/// <summary>
		/// Occures before the dialog is repainted
		/// </summary>
		static partial void BeforePaint();

		/// <summary>
		/// Occures when the softkeys are being defined
		/// </summary>
		static partial void DefineSoftkeys();

		/// <summary>
		/// Occures after the dialog is repainted
		/// </summary>
		static partial void AfterPaint();
		
		/// <summary>
		/// Occures after an event
		/// </summary>
		static partial void EventOccured();

		/// <summary>
		/// Occures when the dialog has closed
		/// </summary>
		static partial void Closed();

		/// <summary>
		/// Occures when the user presses the cmdEingang key
		/// </summary>	
		static partial void cmdEingang_Triggered();

		/// <summary>
		/// Occures when the user presses the cmdEinfach key
		/// </summary>	
		static partial void cmdEinfach_Triggered();

		/// <summary>
		/// Occures when the user presses the cmdAusgang key
		/// </summary>	
		static partial void cmdAusgang_Triggered();

		/// <summary>
		/// Occures when the user presses the cmdSettings key
		/// </summary>	
		static partial void cmdSettings_Triggered();
	}
}




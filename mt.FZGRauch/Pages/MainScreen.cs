﻿using System;
using System.Terminal;
using MT.FZGRauch.Data;
using MT.FZGRauch.Logic;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for the main screen
    /// </summary>
    public static partial class MainScreen
    {

        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        static partial void Initialize()
        {
            int i = Program.FirstLine.Length / 2;
            Program.Title = Program.FirstLine;
            Scale.SelectScale(Scale.ScaleNo.Achsen);
        }

        static partial void cmdEinfach_Triggered()
        {
            Program.ActionPointer = new Program.Action(Einfachwaegung.Show);
            Close();
        }

        static partial void cmdAusgang_Triggered()
        {
            Database.Open();
            if (TransData.SelectAllRows() == 0)
            {
                Display.Popup("Ausgangsw" + string.Chr(228) + "gung", "Keine Eingangsw" + string.Chr(228) + "gung vorhanden");
                Database.CloseTables();
            }
            else
            {
                Program.ActionPointer = new Program.Action(Ausgangswaegung.Show);
                Close();    
            }
            
            
        }

        static partial void cmdEingang_Triggered()
        {
            Program.ActionPointer = new Program.Action(Eingangswaegung.Show);
            Close();
        }

       
        static partial void cmdSettings_Triggered()
        {
            Program.ActionPointer = new Program.Action(Settings.Show);
            Close();
        }

    }
}

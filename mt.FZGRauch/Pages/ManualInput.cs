﻿using System;
using System.Terminal;
using MT.FZGRauch.Data;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for ManualInput
    /// </summary>
    public static partial class ManualInput
    {
        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        ////NICHTS ändern, Compiler dreht durch   txtWarencode muss INDEX 5 haben da sonst der EVENT nicht kommt
        ///
        /// 
        static partial void Initialize()
        {
            Program.Title = "Manuelle Eingabe bei fehlender Erstw" + string.Chr(228) + "gung";
        }

        static partial void BeforePaint()
        {
            txtWarencode.TextChanged += None;
        }
        static partial void Closed()
        {
            txtWarencode.TextChanged += None;
        }

        static void None()
        {
        }

        static partial void AfterPaint()
        {
            txtWarencode.TextChanged += txtWarencode_TextChanged;
        }


        static void txtWarencode_TextChanged()
        {
            bool exist = false;
            if (txtWarencode.Text != "")
            {
                Database.Open();
                exist = TransDB.FindSupplier(txtWarencode.Text);

                if (!exist)
                {
                    Program.ActionPointer = new Program.Action(MainScreen.Show);
                    Close();
                }
                else
                {
                    txtIdentification.Draw(Supplier.Current.LicPlate);
                }
                Database.CloseTables();
            }

        }
        static partial void cmdExit_Triggered()
        {
            Program.ActionPointer = new Program.Action(Ausgangswaegung.Show);
            Close();
        }

        static partial void cmdOk_Triggered()
        {
            if (txtWarencode.Text != "" && int.Parse(txtGewicht.Text) > 0)
            {
                Program.FirstIdentification = txtIdentification.Text;
                Program.FirstWeight = txtGewicht.Text;
                Program.Warencode = txtWarencode.Text;
                Program.ActionPointer = new Program.Action(AusgWmanShow.Show);
                Close();
            }
        }
    }
}

﻿using System;
using System.Terminal;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for AnzWaagscheine
    /// </summary>
    public static partial class PrintSettings
    {
        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        static partial void Initialize()
        {
            cbLocal.Text = (Program.Local > 0) ? "Ja" : "Nein";
            cbLaser.Text = (Program.Laser > 0) ? "Ja" : "Nein";
        }

        static partial void cmdExit_Triggered()
        {
            Program.Local = (cbLocal.Text == "Ja") ? 1 : 0;
            Program.Laser = (cbLaser.Text == "Ja") ? 1 : 0;

            Program.ActionPointer = new Program.Action(Settings.Show);
            Close();
        }


    }
}

﻿using System;
using System.Reflection;
using System.Terminal;
using MT.FZGRauch.Data;
using MT.FZGRauch.Logic;


namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for Printing
    /// </summary>
    public static partial class Printing
    {
        /// <summary>
        /// Stores the label1 control
        /// </summary>					
        private static readonly SharedDataLabel<string> sharedDataLabel1 = new SharedDataLabel<string>(1, 100, 14, "AK0102", "!50", Fonts.Normal16, Colors.Black);
        private static readonly SharedDataLabel<string> sharedDataLabel2 = new SharedDataLabel<string>(2, 80, 57, "AK0105", "!29", Fonts.Normal16, Colors.Black);

        private static KeyCodes key;

        private static void InitializeComponents()
        {
            Display.Clear();
            Display.SetupWeightDisplay(WeightDisplayVisibility.On, WeightDisplayScale.Active, WeightDisplayTare.Tare, WeightDisplayCompress.Uncompress, WeightDisplayScaleSize.Medium, WeightDisplayScaleSize.Medium);

            sharedDataLabel1.Draw();
            sharedDataLabel2.Draw();

            // Update softkeys
            SoftKeys.Clear();

            SoftKeys.Define(Keys.SoftKey1, 100, "\\exit.bmp");
            if (Program.Local == 1)
                SoftKeys.Define(Keys.SoftKey2, 101, "\\report.bmp");
            if (Program.Laser == 1)
                SoftKeys.Define(Keys.SoftKey4, 103, "\\report.bmp");

            SoftKeys.Replace();
        }

        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        private static void ShowDialog()
        {
            bool printLaser_pending = (Program.Laser == 1);
            bool printLocal_pending = (Program.Local == 1);
            bool first_print = true;
            bool print_pending = true;
            string result = "";

            InitializeComponents();
            do
            {
                if (Program.Laser > 0)
                {
                    if (printLaser_pending == true)
                    {
                        Program.Title = "An Server senden...";
                        Program.Hinweis = "          Bitte warten....";
                        result = TcpClient.SendWeigh();
                        printLaser_pending = false;
                        Program.Title = "Nachdruck";
                        if (Program.Local == 0)
                             Program.Hinweis = "           F4 Laser";
                        else
                            Program.Hinweis =  "F2 Lokal / F4 Laser";
                    }
                }

                if (Program.Local > 0)
                {
                    if (printLocal_pending == true)
                    {
                        Program.Title = "    Ausdruck";
                        Program.Hinweis = "Papier in Drucker einlegen";
                        printLocal_pending = false;
                        print_pending = true;
                    }

                    if ((Printer.CheckPrinter() == PrinterStates.Paper) && (print_pending == true))
                    {
                        Program.Hinweis = "Daten an Drucker senden......";
                        RePrint(first_print);
                        Program.Title = "Nachdruck";
                        if (Program.Laser > 0)
                            Program.Hinweis = "F2 Lokal / F4 Laser";
                        else
                            Program.Hinweis = "F2 Lokal";
                        print_pending = false;
                        first_print = false;
                        result = "sogut";
                    }
                }

                key = (KeyCodes)RuntimeScreen.ReadKey();

                if (key == KeyCodes.Softkey02)
                {
                    printLocal_pending = true;
                }
                if (key == KeyCodes.Softkey04)
                {
                    printLaser_pending = true;
                }
            } while (key != KeyCodes.Softkey01);

            if (result.Length > 1)
            {
                SetDateTime(result);
                Database.Open();
                TransDataOut.DeleteId(int.Parse(Program.WaagScheinNrDisp).ToString());
                Database.CloseTables();
            }

            Program.ActionPointer = new Program.Action(MainScreen.Show);
        }

        public static void RePrint(bool first_print)
        {
            string str;
            int i;
            if (Printer.CheckPrinter() == PrinterStates.Paper)
            {

                Printer.FeedPaper();
                if (first_print == false)
                {
                    Printer.WriteLine("\x1b\x21\x10");  // Double Heigth Mode on
                    Printer.WriteLine("     *** DRUCKWIEDERHOLUNG ***");
                    Printer.WriteLine("\x1b\x21\x40");  // Double Heigth Mode off
                    Printer.WriteLine("\n\n");
                }
                File file = new File(3);
                file.Open("\\storage card\\TaskExpert\\Data\\templ.txt", FileAccess.Input);
                Printer.WriteLine("\x1b\x74\x02");
                //file.Open("templ.txt", FileAccess.Input);
                while (!file.AtEndOfStream)
                {
                    file.ReadLine(out str);
                    Printer.WriteLine(str + "\n");
                    Task.Sleep(500);
                }

                file.Close();
                Printer.EjectPaper();
            }
        }

        public static void SetDateTime(string thisdatetime)
        {
            //commands.Add("w xs0111 3\r");
            //commands.Add("w xs0110 7\r");
            //commands.Add("w xs0112 .\r");
            //commands.Add("w xk0111 " + dt.ToString("HH:mm:ss") + "\r");
            //commands.Add("w xk0112 " + dt.ToString("yyyy.MM.dd") + "\r");
            //commands.Add("r xk0112\r");
            //commands.Add("r xk0111\r");
        }

    }

    public enum KeyCodes
    {
        None = 0,
        Softkey01 = 100,
        Softkey02,
        Softkey03,
        Softkey04,
        Softkey05,
        Softkey06,
        Softkey07,
        Softkey08,
        Softkey09,
        Softkey10,
        Softkey11,
        Softkey12,
        Softkey13,
        Softkey14,
        Softkey15
    }

}

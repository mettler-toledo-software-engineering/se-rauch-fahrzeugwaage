﻿using System;
using System.Terminal;
using MT.FZGRauch.Data;
using MT.FZGRauch.Logic;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for Settings
    /// </summary>
    public static partial class Settings
    {
        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        static partial void cmdTransaction_Triggered()
        {
            Database.Open();
            if (TransDataOut.SelectAllRows() == 0)
            {
                Database.CloseTables();
                Display.Popup("Ausgangsw" + string.Chr(228) + "gung", "Keine Transaktion vorhanden");
            }
            else
            {
                Program.ActionPointer = new Program.Action(Transactions.Show);
                Close();
            }
        }

        static partial void cmdExit_Triggered()
        {
            Program.ActionPointer = new Program.Action(MainScreen.Show);
            Close();
        }

        static partial void cmdEichdatum_Triggered()
        {
            Program.ActionPointer = new Program.Action(LastApproval.Show);
            Close();
        }

        static partial void cmdAppQuit_Triggered()
        {
            Environment.Exit();
        }

        static partial void cmdReport_Triggered()
        {
            Program.ActionPointer = new Program.Action(PrintSettings.Show);
            Close();
        }

        static partial void cmdTCP_Triggered()
        {
            Program.ActionPointer = new Program.Action(TCPSettings.Show);
            Close();
        }

        static partial void cmdClients_Triggered()
        {
            Program.ActionPointer = new Program.Action(Artikel.Show);
            Close();
        }

    }
}

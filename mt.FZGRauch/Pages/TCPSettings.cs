﻿using System;
using System.Terminal;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for AnzWaagscheine
    /// </summary>
    public static partial class TCPSettings
    {
        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        static partial void Initialize()
        {
            txtPort.Text = Program.ServerPort.ToString();
            txtIP.Text = Program.ServerIP;
        }

        static partial void cmdExit_Triggered()
        {
            Program.ServerPort = int.Parse(txtPort.Text);
            Program.ServerIP = txtIP.Text;
            Program.ActionPointer = new Program.Action(Settings.Show);
            Close();
        }

        
    }
}

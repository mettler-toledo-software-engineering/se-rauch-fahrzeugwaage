﻿using System.Terminal;
using MT.FZGRauch.Data;
using MT.FZGRauch.Logic;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for Transactions
    /// </summary>
    public static partial class Transactions
    {
       public static bool Show()
        {
            ShowDialog();
            return false;
        }
        static partial void Initialize()
        {
            Program.Title = "Nicht ausgedruckte Transaktionen";
            Program.FirstIdentification = TransDataOut.Current.LicPlate;
            Program.WaagScheinNrDisp = TransDataOut.Current.WaagScheinNummer;
        }

        static partial void cmdTransclr_Triggered()
        {
            Database.CloseTables();
            Program.Title = "Transaktion l" + string.Chr(246) + "schen ?";
            Program.ActionPointer = new Program.Action(YesNo.Show);
            Close();
        }

        static partial void cmdTransprint_Triggered()
        {
            Database.CloseTables();

            Templates.PrintPaper();

            if ((Program.Laser == 0) && (Program.Local == 0))
            {
                Program.Title = "Kein Druck verlangt! L" + string.Chr(246) + "schen ?";
                Program.ActionPointer = new Program.Action(YesNo.Show);
                Close();
            }
            else
            {
                Program.ActionPointer = new Program.Action(Printing.Show);
                Close();
            }
        }

        static partial void cmdExit_Triggered()
        {
            Database.CloseTables();
            Program.ActionPointer = new Program.Action(Settings.Show);
            Close();
        }

        static partial void cmdNext_Triggered()
        {
            if (TransDataOut.NextRow() == 0)
                TransDataOut.SelectAllRows();
            Program.FirstIdentification = TransDataOut.Current.LicPlate;
            Program.WaagScheinNrDisp = TransDataOut.Current.WaagScheinNummer;
            _redraw = true;
        }

        static partial void cmdTransclrs_Triggered()
        {
            Database.CloseTables();
            Program.Title = "Alle Transaktion l" + string.Chr(246) + "schen ?";
            Program.ActionPointer = new Program.Action(YesNo.Show);
            Close();
        }
    }
}

﻿using System;
using System.Terminal;
using MT.FZGRauch.Data;

namespace MT.FZGRauch.Pages
{
    /// <summary>
    /// Interaction logic for YesNo
    /// </summary>
    public static partial class YesNo
    {
        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        static partial void cmdYes_Triggered()
        {
            Database.Open();
            if (Program.Title.IndexOf("Alle") > 0)
            {
                do
                {
                    TransDataOut.DeleteId(TransDataOut.Current.WaagScheinNummer);
                } while (TransDataOut.SelectAllRows() != 0);
            }
            else if (Program.Title.IndexOf("Transaktion") > 0)
            {
                TransDataOut.DeleteId(int.Parse(Program.WaagScheinNrDisp).ToString());
                TransData.DeleteId(int.Parse(Program.WaagScheinNrDisp).ToString());
            }
            else if (Program.Title.IndexOf("Artikel") > 0)
            {
                Supplier.DeleteId(int.Parse(Program.Warencode).ToString());
            }

            Database.CloseTables();
            Program.ActionPointer = new Program.Action(MainScreen.Show);
            Close();
        }

        static partial void cmdNo_Triggered()
        {
            Program.ActionPointer = new Program.Action(MainScreen.Show);
            Close();
        }
    }
}

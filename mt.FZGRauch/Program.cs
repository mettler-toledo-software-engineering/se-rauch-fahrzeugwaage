﻿using System;
using System.Terminal;
using MT.FZGRauch.Data;
using MT.FZGRauch.Logic;
using MT.FZGRauch.Pages;

namespace MT.FZGRauch
{
    static class Program
    {
        /// <summary>
        /// Main entry point of your application
        /// </summary>
        ///       
       //public static readonly SerialPort Prn = new SerialPort(SerialPortHandle, 1, AccessType.Read, 80, '\r', 30000, SerialPortFlags.AutoFlush)
        
        public const double MinLoad = 100;


        [Share(SD.Application.SetupString, 1)]
        public static string FirstRun;
        [Share(SD.Application.SetupString, 2)]
        public static string FirstLine;
        [Share(SD.Application.SetupString, 3)]
        public static string SecondLine;
        [Share(SD.Application.SetupString, 4)]
        public static string Approvel;

        [Share(SD.Application.DynamicString, 1)]
        public static string Version;
        [Share(SD.Application.DynamicString, 2)]
        public static string Title;
        [Share(SD.Application.DynamicString, 3)]
        public static string WaagScheinNrDisp;
        [Share(SD.Application.DynamicString, 5)]
        public static string Hinweis;
        [Share(SD.Application.DynamicString, 6)]
        public static string FirstWeight;
        [Share(SD.Application.DynamicString, 7)]
        public static string FirstTimeStamp;
        [Share(SD.Application.DynamicString, 8)]
        public static string FirstWaagscheinNummer;
        [Share(SD.Application.DynamicString, 9)]
        public static string FirstIdentification;
        [Share(SD.Application.DynamicString, 10)]
        public static string Warencode;

       
        [Share(SD.Application.SetupInteger, 1)]
        public static int WaagscheinNummer;
        [Share(SD.Application.SetupInteger, 2)]
        public static int Local;
        [Share(SD.Application.SetupInteger, 3)]
        public static int AusdruckNummer;
        [Share(SD.Application.SetupInteger, 4)]
        public static int Laser;
        [Share(SD.Application.SetupInteger, 5)]
        public static int ServerPort;


        [Share("np0106")]
        public static string ServerIP;

      
        [Share("TX0105", FireEvents = true)]  // Fehler in Compiler fixen damit Events möglich werden
        public static bool dummy;  //NICHTS ändern, Compiler dreht durch

        public delegate bool Action();
        public static Action ActionPointer;
        public static string Identification;
        public static bool FirstWeighingExist;
       
        static void Main() 
        {  Task.Sleep(200);
            Version = "V1.3";
            string programTitle = "Rauch Trading AG " + Version;
            if (FirstRun != programTitle)
            {
                FirstRun = programTitle;
                //TransDB.Init();
                //WaagscheinNummer = 0;
                //AusdruckNummer = 0;
                Program.FirstLine = "Rauch Trading AG ";
                Program.SecondLine = "9443 Widnau";
                Program.Approvel = "IND780/T2206";
            }
            Timer.InitTimerEvent();
            Timer.TimerElapsed += new Timer.TimerElapsedHandler(Timer_TimerElapsed);
            Timer.Start(500);
            

            RuntimeScreen.Initialize();
            ActionPointer = new Action(MainScreen.Show);
            while (!ActionPointer()) ;
        }

        static void Timer_TimerElapsed()
        {
           
        }
    }
}
